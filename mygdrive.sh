#!/bin/bash

# Here we read in the location of the file with FileIDs and Folder names...

myfile=${1}

while IFS= read -r line; do 
	array=( ${line} ); 
	echo "Downloading folder: ${array[1]}........."; 
	/home/chandler/gdrive --service-account service_account_mydrive_downloader.json download --recursive ${array[0]}; 
done < ${myfile}
